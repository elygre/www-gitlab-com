---
layout: markdown_page
title: "2016 Performance Reviews"
---


### Performance Review Process

Refer to the following Q&A to see who should participate in the 2016 performance review process, when reviews must be completed and how the information collected will be used. If you did not receive an invitation from Lattice, and you believe you should have after reading the information below, please email People Ops so we can resolve any issues quickly.

**Q: Why are we doing 2016 performance reviews?**

**A:** We will use 2016 performance reviews as a guideline for awarding 2017 [merit increases](/handbook/people-operations/global-compensation/#performance-based-increase-plan).

**Q: Who will participate in this performance review process?**

**A:** Team members who were hired before October 1, 2016 and meet all eligibility requirements for a 2017 merit increase will complete a Self Review and receive a Manager’s Review. Team members hired on or after October 1, 2016 or who are not eligible for a merit increase will participate in the 2017 performance cycle, which will kick-off in June 2017.

**Q: What will performance reviews be based on?**

**A:** Reviews will include three questions that will be rated by both the individual and their manager and two questions focused on recognizing strengths and development opportunities.

Individual Contributor (IC) Self Reviews include these five questions:
1. To what degree did you demonstrate GitLab’s core values?
2. To what degree did you demonstrate the requirements in your job description?
3. Considering your role and the 2016 strategic plan, to what degree did you contribute to your department’s goal?
4. In what areas did you demonstrate the most growth in 2016?
5. Looking forward, in what areas would you most like to develop your knowledge, skills and abilities?

Manager Reviews for ICs include the same five questions and an overall performance summary.

Manager Self Reviews include these six questions:
1. To what degree did you demonstrate GitLab’s core values?
2. To what degree did you demonstrate the requirements in your job description?
3. Considering your role and the 2016 strategic plan, to what degree did you contribute to your department’s goal?
4. Considering the actions below, how effective were you in developing a high performing team? Did you have regularly scheduled feedback meetings with each team member?
Did you facilitate development opportunities, manage performance issues in a timely manner, identify strengths and weaknesses in the team and provide constructive guidance and coaching to team members?
5. In what areas did you demonstrate the most growth in 2016?
6. Looking forward, in what areas would you most like to develop your knowledge, skills and abilities?

Manager reviews for Managers included the same six questions and an overall performance summary.

**Q: How long should the response to each question be?**

**A:** The first three questions in the IC self and manager’s review and the first four questions in manager self and manager’s review require a comment and a rating response. Comments should be to the point and include specific examples which reinforce the selected rating response.  

**Q: What are the rating responses?**

**A:** Ratings options include:

|Performance Ratings|Definition / Example Behaviors|
|:-----:|:-----|
|Below Expectations|- Requires constant supervision and reminders to complete assignments|
| |- Rarely produces work that meets expected standards|
| |- Overall contribution to team is very low|
| |- Communication is poor|
| |- Not meeting some or all of the requirements of the job description|
|
|Met Expectations|- Needs minimal to no support from manager to complete assignments|
| |- Work is of an acceptable quality with minimal errors|
| |- Contributes to team discussions and idea generation|
| |- Fulfills requirements of the job description|
|
|Exceeded Expectations|- Needs no support from manager to complete assignments|
| |- Work is of an exceptional quality most of the time|
| |- Seeks out opportunities to create innovative solutions|
| |- Exceeds requirements of the job description|
|
|Greatly Exceeded Expectations|- Works independently to complete all assignments|
| |- Delivers exceptional quality consistently|
| |- Takes the lead in creating innovative solutions|
| |- Greatly exceeds requirements of the job description|
|
|Truly Outstanding|- Takes the lead on complex work assignments|
| |- Drives change in a positive way|
| |- Contributes to team success and development|
| |- Is seen as someone who sets the standard for others to follow|
| |- Consistently adds value well beyond job requirements|
|||


**Q: Do individuals rate their own performance?**

**A:** Yes, both the self review and manager’s review require a rating for performance questions.  The manager’s review also includes an overall performance rating which will factor into the 2017 increase guidelines.

**Q: When are performance reviews due?**

**A:** All reviews must be completed by end of day (EST) Wednesday, April 19, 2017.

**Q: Will we have an opportunity to review and compare self and manager reviews?**

**A:** Yes, managers will receive a copy of self reviews as they are completed. After all managers are finished, we will close the 2016 review cycle and share manager reviews with their direct reports to facilitate open and meaningful dialog around alignment and differences between manager and self reviews. Performance gaps identified should generate productive development conversations.

**Managers:**
In cases where you’ve identified your top performer, we should learn from what makes that person successful to share with others.
In cases where below average performance is identified, you should plan to deliver a PIP to clearly identify performance gaps and expected changes.

**Q: Why link performance to the increase process?**

**A:** Cost of living adjustments (COLA) encourage participation, generate mediocrity and lead to the disengagement of high performers, while performance-based (merit) increases reward results and motivate continued development and higher achievements.

**Q: When will we hear about 2017 [merit increases](/handbook/people-operations/global-compensation/#performance-based-increase-plan)?**

**A:** After the performance process is complete, we will ask managers to weigh in on suggested merit increase guidelines. Management recommendations will then be reviewed by each division executive and the CEO for final approval. If we stick to the established deadlines, we anticipate increases will be effective May 1, 2017.  

_**To assist with the performance review steps in Lattice:**_

Once People Ops launches a review cycle you should receive an email invitation from Lattice to begin the review process. From the email notification:
* Click ***Start your Review*** (at the bottom of the email)
* Login to Lattice
* If not immediately directed to the Reviews tab:
* Click ***Reviews*** (at the top of the Home screen) ***=> Review Cycles =>*** ***Active***
* Click ***Perform Reviews*** begin Self Review
* All questions require an answer
* Answers will auto save as you complete the review form
* Click ***Save and Exit*** (at the top right corner) to save your work and come back later to finish your review.
* Click ***Submit Review*** when finished
* Reviews are editable until the review cycle is closed
* People Ops will publish a notice before closing a review cycle
* Managers can view self reviews as they are submitted by their direct reports

Once People Ops closes a review cycle:
* Managers will see a review packet for their direct reports
* Click ***Share with***  ____ to share the review packet with your direct reports (the review will not be shared until you take action)
* You can also download a PDF version of your direct report’s review packet
* Direct reports will receive an email from Lattice when a review packet has been sent to you.

Late Reviewers (alternate manager):
* People Ops can add or update reviewers (past or present managers) during an active review cycle
* If you are added as a ***Late Reviewer:***
* You will receive an email from Lattice asking that you provide feedback on a particular individual
* To begin your review, click the link provided in the email

Chat Feature:
* You have access to Lattice’s Customer Success team by clicking the ***Chat*** button in the bottom right hand corner of the screen (looks like a document with a smile)

Lattice Help Center Articles:
* [What to expect as a reviewee during a review cycle](https://help.latticehq.com/reviews/participating-in-a-review-cycle/what-to-expect-as-a-reviewee)
* [What to expect as a manager during a review cycle](https://help.latticehq.com/reviews/participating-in-a-review-cycle/what-to-expect-as-a-manager-during-a-review-cycle)

If you have any questions or concerns as you go through the 2016 performance cycle, please reach out to People Ops.
