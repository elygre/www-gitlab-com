---
layout: job_page
title: "Security Engineer"
---

A Security Specialist is a [Developer](https://about.gitlab.com/positions/developer/) who focuses on ensuring that
GitLab and associated applications are as secure as possible. The Security Specialist works in close collaboration with the Security Lead and has the following set of skills, experience, and responsibilities:

1. Technical Skills
    * Development experience with Ruby on Rails. This position does not require
      senior-level development experience but the applicant should be very familiar
      with common security libraries, security controls, and common security flaws
      that apply to Ruby on Rails applications. 
    * Ability to discover and patch SQLi, XSS, CSRF, SSRF, authentication and
      authorization flaws, and other web-based security vulnerabilities (OWASP
      Top 10 and beyond).
    * Knowledge of common authentication technologies including OAuth, SAML, CAs,
      OTP/TOTP.
    * Knowledge of browser-based security controls such as CSP, HSTS, XFO.
    * Experience with standard web application security tools such as Arachni,
      Brakeman, and BurpSuite.
2. Code quality
    * Proactively identifying and reducing security risks.
    * Finding and removing outdated and vulnerable code and code libraries.
3. Communication
    * Consult with other developers and product managers to analyze and propose
      application security standards, methods, and architectures.
    * Handle communications with independent vulnerability researchers and design
      appropriate mitigation strategies for reported vulnerabilities.
    * Educate other developers on secure coding best practices.
    * Ability to professionally handle communications with outside researchers,
      users, and customers.
    * Ability to communicate clearly on technical issues.
4. Performance & Scalability
    * An understanding of how to write code that is not only secure but scales
      to a large number of users and systems.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

    * Selected candidates will be invited to schedule a 45 minute [screening call](/handbook/hiring/#screening-call) with a Recruiter
    * Next, candidates will be invited to schedule a 45 minute [technical interview](/jobs/#technical-interview) with the [Security Lead] (https://about.gitlab.com/jobs/security-engineer/)
    * Candidates will then be invited to schedule a 45 minute interview with our Interim VP of Infrastructure
    * Candidates will be invited to schedule a one hour interview with our VP of Engineering
    * Finally, candidates will have a 50 minute interview with our CEO
    * Successful candidates will subsequently be made an offer via email
