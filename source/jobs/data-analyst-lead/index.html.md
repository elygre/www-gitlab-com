---
layout: job_page
title: "Data analyst lead"
---

## Responsibilities

### Goal

Effectively focus our efforts in product, marketing, sales, and customer success based on customer analytics.

### Analysis

Below are examples of the type of analytics we might want to do:

1. Sales enablement: convert CE to EE, upsell EE Starter to EE Premium, convert GitLab.com groups to paying groups or EE Starters, etc.
1. Sales: Incremental ACV, TCV, ARR, Renewal rate, Win rate, Time to close, Pipeline created, Pipeline Requirement Projection, [Sales efficiency (above 0.8)](http://tomtunguz.com/magic-numbers/), [Pipeline coverage (above 3)](https://pipetop.com/saas-glossary/sales-terms/sales-pipeline-coverage/)
1. Customer Success: measure and enable feature adoption
1. Peopleops: Inbound applications per week, Interviews per applicant, eNPS score of team members, Applicant score per requirement and value, Declined applicant NPS score, offer accept rate, Accepted offers, Cycle time (apply to accepted offer), Terminations (Voluntary regrettable, Voluntary PIP, Involuntary), % of people on PIPs
1. Marketing: Campaigns, Visitors, Downloads, MQL, SQL, Program spend / Total spend (above 0.5), conversion percentages and cycle time per channel
1. Engineering: Merge requests (total, community, ours), Cycle time, Time to review, Bug fixes (cherrypick into stable)
1. Product: Money made with new features, Retweets for release tweet, Usage per feature, usage stats, bottlenecks to increased customer sophistication
1. Finance: Order to cash, Runway (above 12 months), [40 rule (above 40%)](http://www.feld.com/archives/2015/02/rule-40-healthy-saas-company.html), [Magic number (above 0.7)](https://davidcummings.org/2009/09/21/saas-magic-number/)

### Data sources

1. Salesforce
1. Zuora
1. Marketo
1. Zendesk
1. NetSuite
1. Mailchimp
1. Google Analytics
1. Discover.org
1. Applicant Tracking System
1. GitLab version check
1. GitLab usage ping
1. [GitLab.com](https://about.gitlab.com/handbook/engineering/workflow/#getting-data-about-gitlabcom)

We want a single data warehouse and a central data model. We bring all relevant data to single storage place. Make it platform agnostic, so for example do not build it into Salesforce. We want to build data model to be used consistently across tools and teams. For example something as simple as unique customer ID, product or feature names/codes.

### Tools

These are still to be decided, some suggestions below.

1. Ingestion: Fivetran or Alooma
1. Warehouse: BigQuery
1. Display: Looker, Redash, or Metabase

### Reporting

You will report to the CFO. You'll work with Product, Marketing, and Sales leadership to coordinate the central data model.

### Requirements

TODO